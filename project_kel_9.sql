/*
SQLyog Community v13.2.0 (64 bit)
MySQL - 10.4.25-MariaDB : Database - project_kel_9
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`project_kel_9` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `project_kel_9`;

/*Table structure for table `data_barang` */

DROP TABLE IF EXISTS `data_barang`;

CREATE TABLE `data_barang` (
  `kode_barang` char(5) NOT NULL,
  `nama_barang` varchar(25) DEFAULT NULL,
  `jumlah` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`kode_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `data_barang` */

insert  into `data_barang`(`kode_barang`,`nama_barang`,`jumlah`) values 
('001','buku tulis','112'),
('002','pena','111'),
('003','buku gambar','113'),
('004','majalah','114');

/*Table structure for table `data_pengajuan` */

DROP TABLE IF EXISTS `data_pengajuan`;

CREATE TABLE `data_pengajuan` (
  `kode_pengajuan` char(5) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `npm_peminjam` char(10) DEFAULT NULL,
  `nama_peminjam` varchar(20) DEFAULT NULL,
  `prodi` varchar(20) DEFAULT NULL,
  `no_handphone` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`kode_pengajuan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `data_pengajuan` */

insert  into `data_pengajuan`(`kode_pengajuan`,`tanggal`,`npm_peminjam`,`nama_peminjam`,`prodi`,`no_handphone`) values 
('001','0000-00-00','20311413','samuelbs','sistem informasi','085768167454');

/*Table structure for table `data_pengajuan_detail` */

DROP TABLE IF EXISTS `data_pengajuan_detail`;

CREATE TABLE `data_pengajuan_detail` (
  `kode_pengajuan` char(5) NOT NULL,
  `kode_barang` char(5) NOT NULL,
  `jumlah` varchar(20) DEFAULT NULL,
  KEY `kode_pengajuan` (`kode_pengajuan`),
  CONSTRAINT `data_pengajuan_detail_ibfk_1` FOREIGN KEY (`kode_pengajuan`) REFERENCES `data_pengajuan` (`kode_pengajuan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `data_pengajuan_detail` */

insert  into `data_pengajuan_detail`(`kode_pengajuan`,`kode_barang`,`jumlah`) values 
('001','002','109');

/*Table structure for table `data_pengembalian` */

DROP TABLE IF EXISTS `data_pengembalian`;

CREATE TABLE `data_pengembalian` (
  `kode_pengembalian` char(5) NOT NULL,
  `kode_pengajuan` char(5) NOT NULL,
  `tanggal_kembali` date DEFAULT NULL,
  PRIMARY KEY (`kode_pengembalian`),
  KEY `kode_pengajuan` (`kode_pengajuan`),
  CONSTRAINT `data_pengembalian_ibfk_1` FOREIGN KEY (`kode_pengajuan`) REFERENCES `data_pengajuan` (`kode_pengajuan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `data_pengembalian` */

/*Table structure for table `data_pengembalian_detail` */

DROP TABLE IF EXISTS `data_pengembalian_detail`;

CREATE TABLE `data_pengembalian_detail` (
  `kode_pengembalian_detail` char(5) NOT NULL,
  `kode_pengembalian` char(5) NOT NULL,
  `jumlah` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`kode_pengembalian_detail`),
  KEY `kode_pengembalian` (`kode_pengembalian`),
  CONSTRAINT `data_pengembalian_detail_ibfk_1` FOREIGN KEY (`kode_pengembalian`) REFERENCES `data_pengembalian` (`kode_pengembalian`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `data_pengembalian_detail` */

/*Table structure for table `mahasiswa` */

DROP TABLE IF EXISTS `mahasiswa`;

CREATE TABLE `mahasiswa` (
  `mhsNpm` char(12) NOT NULL,
  `mhsNama` varchar(255) DEFAULT NULL,
  `mhsAlamat` text DEFAULT NULL,
  `mhsFakultas` varchar(255) DEFAULT NULL,
  `mhsProdi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`mhsNpm`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `mahasiswa` */

insert  into `mahasiswa`(`mhsNpm`,`mhsNama`,`mhsAlamat`,`mhsFakultas`,`mhsProdi`) values 
('11312138','Mahasiswa 3','alamat mahasiswa 3','fakultas mahasiswa 3','prodi mahasiswa 3'),
('20311059','DickyFernandaSebastian','alamat mahasiswa 2\r\n','Teknik','Teknik Elektro'),
('20311413','SamuelBudiSetiawan','alamat mahasiswa 1\r\n','Teknik','Sistem Informasi');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `user` */

insert  into `user`(`id_user`,`name`,`username`,`pass`,`role`) values 
(1,'samuelbudisetiawan','samuelbs','samuelbs','Admin'),
(2,'admin2','Admin2','admin','Admin'),
(4,'tess1','tes11','22daf1a39b6e5ea16554f59e472d96f6','Admin');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
